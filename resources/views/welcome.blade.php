@extends('base')

@section('content')
<div class="mb-8">
    <x-links.cta-green href="{{ route('aggregations.create') }}" label="Neue Notiz anlegen"/>
</div>

<div class="md:flex">
    <div id="aggregations-list" class="grid w-full">
        <h2 class="mb-2 text-2xl">Bisherige Notizen</h2>
        <input type="text" class="p-4 mb-4 text-xl border-b border-gray-600 search" placeholder="Notizen durchsuchen">
        <ul class="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 list">
        @php
            $year = '';
            $month = '';
        @endphp
        @foreach ($aggregations_chron as $aggregation)
            @if ($aggregation->label)
                @php
                    if($year != date('Y', strtotime($aggregation->created_at))) {
                        $year = date('Y', strtotime($aggregation->created_at));
                        print('<li class="col-span-1 sm:col-span-2 md:col-span-3"><h2 class="mb-0 text-4xl font-bold">'.$year.'</h2></li>');
                    }
                    if($month != date('M', strtotime($aggregation->created_at))) {
                        $month = date('M', strtotime($aggregation->created_at));
                        print('<li class="col-span-1 sm:col-span-2 md:col-span-3"><h3 class="mb-2 text-2xl font-bold">'.$month.'</h3></li>');
                    }
                @endphp
                <li>
                    
                    <a href="{{ route('aggregations.edit', [$aggregation]) }}" class="block overflow-hidden mb-2 rounded-xl shadow-2xl">

                        @if ($aggregation->documents->first())
                            <img class="mt-2 rounded-tl-xl rounded-tr-xl" loading="lazy" src="/{{ 'storage/' . $aggregation->documents->first()->base_path . '/' . $aggregation->documents->first()->file_name }}" alt="{{ $aggregation->documents->first()->file_name }}"/>
                            <div class="px-4 py-2">
                                <span class="block mt-1 text-xs italic leading-snug">{{ $aggregation->documents->first()->comment ?? '' }}</span>
                            </div>
                        @endif

                        <span class="hidden comments">@foreach ($aggregation->documents as $image){{ $image->comment ?? '' }} @endforeach</span>
                        
                        <div class="p-4">
                            <span class="block font-bold break-all signatures hover:underline">{{ $aggregation->signatures }}</span>
                            <span class="block text-xs leading-relaxed break-all date">{{ date('d. M Y - H:i', strtotime($aggregation->created_at)) }}</span>
                        </div>
                    </a>
                </li>
            @endif
        @endforeach
        </ul>
    </div>
</div>

@endsection

@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <script>

        document.addEventListener('DOMContentLoaded', () => {
            var searchable_list = new List('aggregations-list', {
                valueNames: ['signatures', 'date', 'comments']
            });
        });

    </script>
@endsection
