<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIA Restoration Notes</title>

    <link href="/styles.css" rel="stylesheet">
    
    @yield('styles')

</head>
<body class="overflow-x-hidden p-4">

    <h1 class="text-3xl font-bold"><a href="/">PIA Restoration</a></h1>
    <p class="mb-4 w-full text-xs md:w-1/2">Mit diesem Werkzeug lassen sich Notizen, Fotos und andere Dokumente an Signaturen hängen. Ein zukünftigen Werkzeug wird diese Signaturen auslesen und die Dokumente mit den digitalisierten Objekten verknüpfen.</p>

    @yield('content')
    
    <a href="/" class="fixed top-2 right-2">
        <img class="w-10" src="{{ asset('pia-rat.svg') }}" alt="PIA Logo Rat">
    </a>

    <!--<script defer src="https://unpkg.com/@alpinejs/persist@3.x.x/dist/cdn.min.js"></script>-->
    <!--<script defer src="https://unpkg.com/@alpinejs/intersect@3.x.x/dist/cdn.min.js"></script>-->
    <!-- <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script> -->

    @yield('scripts')

</body>
</html>